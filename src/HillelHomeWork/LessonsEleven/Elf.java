package HillelHomeWork.LessonsEleven;

public class Elf extends Warrior{

    private int height;
    private  int age;
    private String gender;
    private int strength;
    private int defenceStrength;

    public Elf(int height, int age, String gender, int strength, int defenceStrength) {
        this.height = height;
        this.age = age;
        this.gender = gender;
        this.strength = strength;
        this.defenceStrength = defenceStrength;
    }

    public void run() {
        System.out.println("I get order RUN!!!");
    }

    public void punch() {
        System.out.println("Hit: " + strength + "dmg!");
    }

    public void eat() {
        System.out.println("Time to eat. Frodo and Sam take all the bread! But I have these acorns.");
    }

    @Override
    public String specifications(){
        return "Hi! I am elf! My height is " + height + ", age is " + age + ", gender is " + gender + " and my strength is " + strength + ".";
    }

    public int getStrength() {
        return strength;
    }

    @Override
    public void attack() {
        punch();
    }

    @Override
    public void defence() {
        System.out.println("DEFENCE! Power defence is " + defenceStrength + ".");
    }
}
