package HillelHomeWork.LessonsEleven;

public class ElfSwordsman extends Elf implements FasterRunable{

    private int swordStrength;

    public ElfSwordsman(int height, int age, String gender, int strength, int defenceStrength, int swordStrength) {
        super(height, age, gender, strength, defenceStrength);
        this.swordStrength = swordStrength;
    }

    @Override
    public void punch() {
        System.out.println("Hit: " + (super.getStrength() + swordStrength) + "dmg!");
    }

    public int getSwordStrength() {
        return swordStrength;
    }

    @Override
    public String specifications() {
        return super.specifications() + " Also i have a sword! My sword strength is " + swordStrength;
    }

    @Override
    public void fasterRun() {
        System.out.println("I faster fast BOY!");
    }
}
