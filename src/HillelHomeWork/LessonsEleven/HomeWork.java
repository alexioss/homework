package HillelHomeWork.LessonsEleven;

public class HomeWork {

    public static void main(String[] args) {

        Elf testElf = new Elf(190, 125, "Male", 24, 14);
        Dwarf testDwarf = new Dwarf(117, 45, "Female", 74, 35);
        testElf.eat();
        testElf.run();
        testDwarf.eat();
        testDwarf.run();
        System.out.println();

        Warrior[] armyOfElfAndDwarf = new Warrior[15];
        armyOfElfAndDwarf[0] = new Elf(190, 125, "Male", 24, 14);
        armyOfElfAndDwarf[1] = new Elf(176, 221, "Female", 18, 14);
        armyOfElfAndDwarf[2] = new ElfArcher(189, 165, "Male", 21, 14, 45);
        armyOfElfAndDwarf[3] = new ElfArcher(187, 90, "Female", 23, 14, 45);
        armyOfElfAndDwarf[4] = new ElfArcher(192, 78, "Male", 19, 14, 45);
        armyOfElfAndDwarf[5] = new ElfSwordsman(190, 331, "Female", 27, 14, 55);
        armyOfElfAndDwarf[6] = new ElfSwordsman(190, 442, "Male", 30, 14, 55);
        armyOfElfAndDwarf[7] = new ElfSwordsman(190, 565, "Female", 32, 14, 55);
        armyOfElfAndDwarf[8] = new ElfSwordsman(190, 669, "Male", 34, 14, 55);
        armyOfElfAndDwarf[9] = new ElfSwordsman(190, 719, "Female", 40, 14, 55);
        armyOfElfAndDwarf[10] = new Dwarf(120, 57, "Female", 77, 35);
        armyOfElfAndDwarf[11] = new Dwarf(121, 68, "Female", 76, 35);
        armyOfElfAndDwarf[12] = new Dwarf(115, 53, "Female", 70, 35);
        armyOfElfAndDwarf[13] = new Dwarf(125, 61, "Female", 79, 35);
        armyOfElfAndDwarf[14] = new Dwarf(117, 45, "Female", 74, 35);

        HomeWork homeWork = new HomeWork();
        homeWork.showAllWarrior(armyOfElfAndDwarf);
        System.out.println();
        homeWork.allArmyAttack(armyOfElfAndDwarf);
        System.out.println();
        homeWork.allArmyDefence(armyOfElfAndDwarf);
        System.out.println();
        homeWork.armyCanJumpAndFasterRunAttack(armyOfElfAndDwarf);

        Jumpable[] armyCanOnlyJump = new Jumpable[homeWork.countOnlyJumpArmy(armyOfElfAndDwarf)];
        FasterRunable[] armyCanOnlyFastRun = new FasterRunable[homeWork.countOnlyFasterRunArmy(armyOfElfAndDwarf)];

        homeWork.fillArmyCanOnlyJump(armyCanOnlyJump, armyOfElfAndDwarf);
        homeWork.fillArmyCanOnlyFastRun(armyCanOnlyFastRun, armyOfElfAndDwarf);

        System.out.println();
        homeWork.armyJump(armyCanOnlyJump);
        System.out.println();
        homeWork.armyFastRun(armyCanOnlyFastRun);


    }

    public void armyFastRun(FasterRunable[] fasterRunables){
        for (FasterRunable element: fasterRunables) {
            element.fasterRun();
        }
    }

    public void armyJump(Jumpable[] jumpables){
        for (Jumpable element: jumpables) {
            element.jump();
        }
    }

    public void fillArmyCanOnlyFastRun(FasterRunable[] fasterRunables, Warrior[] warriors) {
        for (int i = 0; i < fasterRunables.length; i++) {
            for (Warrior warrior : warriors) {
                if (warrior instanceof Jumpable) {
                    continue;
                } else {
                    if (warrior instanceof FasterRunable) {
                        fasterRunables[i] = (FasterRunable) warrior;
                    }
                }
            }
        }
    }

    public void fillArmyCanOnlyJump(Jumpable[] jumpables, Warrior[] warriors) {
        for (int i = 0; i < jumpables.length; i++) {
            for (Warrior warrior : warriors) {
                if (warrior instanceof FasterRunable) {
                    continue;
                } else {
                    if (warrior instanceof Jumpable) {
                        jumpables[i] = (Jumpable) warrior;
                    }
                }
            }
        }
    }

        public int countOnlyFasterRunArmy (Warrior[]warriors){
            int i = 0;
            for (Warrior element : warriors) {
                if (element instanceof Jumpable) {
                    continue;
                } else {
                    if (element instanceof FasterRunable) {
                        i++;
                    }
                }
            }
            return i;
        }

        public int countOnlyJumpArmy (Warrior[]warriors){
            int i = 0;
            for (Warrior element : warriors) {
                if (element instanceof FasterRunable) {
                    continue;
                } else {
                    if (element instanceof Jumpable) {
                        i++;
                    }
                }
            }
            return i;
        }

        public void armyCanJumpAndFasterRunAttack (Warrior[]warriors){
            for (Warrior element : warriors) {
                if ((element instanceof Jumpable) || (element instanceof FasterRunable)) {
                    element.attack();
                }
            }
        }

        public void allArmyDefence (Warrior[]warriors){
            for (Warrior element : warriors) {
                element.defence();
            }
        }

        public void allArmyAttack (Warrior[]warriors){
            for (Warrior element : warriors) {
                element.attack();
            }
        }

        public void showAllWarrior (Warrior[]warriors){
            for (Warrior element : warriors) {
                System.out.println(element.specifications());
            }
        }
    }
