package HillelHomeWork.LessonsEleven;

public abstract class Warrior {

     public abstract void attack();
     public abstract void defence();
     public abstract String specifications();

}
