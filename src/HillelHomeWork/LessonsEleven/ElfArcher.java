package HillelHomeWork.LessonsEleven;

public class ElfArcher extends Elf implements FasterRunable,Jumpable{

    private int bowStrength;

    public ElfArcher(int height, int age, String gender, int strength, int defenceStrength, int bowStrength) {
        super(height, age, gender, strength, defenceStrength);
        this.bowStrength = bowStrength;
    }

    @Override
    public void punch() {
        System.out.println("Hit: " + (super.getStrength() + bowStrength) + "dmg!");
    }

    @Override
    public String specifications() {
        return super.specifications() + " Also i have a BOW! My bow strength is " + bowStrength;
    }


    @Override
    public void fasterRun() {
        System.out.println("I am fast like a storm!");
    }

    @Override
    public void jump() {
        System.out.println("I use jump to relocate!");
    }
}
