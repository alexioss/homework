package HillelHomeWork.LessonsEleven;

public class Dwarf extends Warrior implements Jumpable{

    private int height;
    private int age;
    private String gender;
    private int strength;
    private int defenceStrength;

    public Dwarf(int height, int age, String gender, int strength, int defenceStrength) {
        this.height = height;
        this.age = age;
        this.gender = gender;
        this.strength = strength;
        this.defenceStrength = defenceStrength;
    }

    public void run() {
        System.out.println("Run for this goblin!");
    }

    public void punch() {
        System.out.println("Hit: " + strength + "dmg!");
    }

    public void eat() {
        System.out.println("You say EAT?!! Davi only drink!!");
    }

    @Override
    public String specifications(){
        return "Oi! I am dwarf! We all have axe! My height is " + height + ", age is " + age + ", gender is " + gender + " and my strength is " + strength + ".";
    }

    @Override
    public void attack() {
        punch();
    }

    @Override
    public void defence() {
        System.out.println("DEFENCE! Power defence is " + defenceStrength + ".");
    }

    @Override
    public void jump() {
        System.out.println("Really? I must jump? OK!");
    }
}
