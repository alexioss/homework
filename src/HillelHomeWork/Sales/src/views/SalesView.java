package HillelHomeWork.Sales.src.views;


import HillelHomeWork.Sales.src.models.Product;
import HillelHomeWork.Sales.src.utils.Validator;
import java.util.Scanner;

// View
public class SalesView {

    String name;
    int quantity;
    double price;
    Scanner scanner;
    Product product;

    public SalesView(Product product) {
        this.product = product;
    }

    public void getInputs() {

        scanner = new Scanner(System.in);

        System.out.println("Введите наименование товара: ");
        name = Validator.validateName(scanner);
        product.setName(name);

        System.out.println("Введите количество: ");
        quantity = Validator.validateQuantityInput(scanner);
        product.setQuantity(quantity);


        System.out.println("Введите цену: ");
        price = Validator.validatePriceInput(scanner);
        product.setPrice(price);

        scanner.close();
    }

    public void getOutput(String productName, double income, double tax, double incomeWithoutTax) {
        System.out.println("Наименование товара: " + productName);
        System.out.println("Общий доход (грн.): " + income);
        System.out.println("Сумма налога (грн.): " + tax);
        System.out.println("Чистый доход (грн.): " + incomeWithoutTax);
    }
}
