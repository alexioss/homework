package HillelHomeWork.Sales.src;

import HillelHomeWork.Sales.src.controllers.ProductController;
import HillelHomeWork.Sales.src.models.Product;
import HillelHomeWork.Sales.src.views.SalesView;

// Входная точка в программу/приложение
public class App {

    public static void main(String[] args) {

        Product model = new Product();
        SalesView view = new SalesView(model);
        ProductController controller = new ProductController(model, view);
        controller.runApp();

    }
}
