package HillelHomeWork.Sales.src.models;


import HillelHomeWork.Sales.src.base.Fiscal;
import HillelHomeWork.Sales.src.base.Income;

// Model.
public class Product implements Fiscal, Income {

    private String name;
    private int quantity;
    private double price;
    private final double TAX_RATE = 0.05;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public double countIncome(int quantity, double price) {
        return quantity*price;
    }

    @Override
    public double taxCount(double income) {
        return income*TAX_RATE;
    }

    @Override
    public double countIncome(double income, double tax) {
        return income - tax;
    }

}
