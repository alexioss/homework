package HillelHomeWork.Sales.src.controllers;

import HillelHomeWork.Sales.src.models.Product;
import HillelHomeWork.Sales.src.views.SalesView;
import static HillelHomeWork.Sales.src.utils.Rounder.*;

// Controller
public class ProductController {

    Product model;
    SalesView view;

    public ProductController(Product model, SalesView view) {
        this.model = model;
        this.view = view;
    }

    public void runApp() {

        view.getInputs();

        String productName = model.getName();
        double income = roundValue(model.countIncome(model.getQuantity(), model.getPrice()));
        double tax= roundValue(model.taxCount(income));
        double incomeWithoutTax = roundValue(model.countIncome(income, tax));

        view.getOutput(productName, income, tax, incomeWithoutTax);
    }
}
