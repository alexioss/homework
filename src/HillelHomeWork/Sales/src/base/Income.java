package HillelHomeWork.Sales.src.base;

public interface Income {

    double countIncome(int quantity, double price);

    double countIncome(double income, double tax);
}
