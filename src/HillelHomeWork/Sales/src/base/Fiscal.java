package HillelHomeWork.Sales.src.base;

public interface Fiscal {

    double taxCount(double income);
}
