package HillelHomeWork.Sales.src.utils;


public class Rounder {

    public static double roundValue(double value){
        return Math.round(value*100)/100D;
    }
}
