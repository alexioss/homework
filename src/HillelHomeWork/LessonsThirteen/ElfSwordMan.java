package HillelHomeWork.LessonsThirteen;

public class ElfSwordMan extends Elf {

    private final double EFFICIENT_REDUCE = 0.8;

    private int countDogeAttack;

    public ElfSwordMan(int livePoints) {
        super(livePoints);
        countDogeAttack = 3;
    }

    @Override
    public int attack() {
        return (int) (super.attack() * EFFICIENT_REDUCE);
    }

    @Override
    public void defence(int attackPoints) {
        if (countDogeAttack == 0) {
            super.defence(attackPoints);
            return;
        }
        countDogeAttack--;
    }
}
