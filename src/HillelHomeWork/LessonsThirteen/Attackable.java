package HillelHomeWork.LessonsThirteen;

public interface Attackable {

    int attack();

}
