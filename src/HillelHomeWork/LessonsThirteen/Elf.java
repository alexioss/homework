package HillelHomeWork.LessonsThirteen;

public class Elf extends Warrior implements Attackable, Defenceable {

    private int livePoints;

    public Elf(int livePoints) {
        this.livePoints = livePoints;
    }

    @Override
    public String toString() {
        return "Elf{" +
                "healthState=" + healthState +
                ", livePoints=" + livePoints +
                ", forceLevel=" + forceLevel +
                ", defenceLevel=" + defenceLevel +
                '}';
    }

    @Override
    public int attack() {
        return forceLevel;
    }

    @Override
    public void defence(int attackPoints) {
        int[] localArray = calculateDefenceLevelAndLivePoint(attackPoints, defenceLevel, livePoints);
        defenceLevel = localArray[0];
        livePoints = localArray[1];
        changeHealthState(livePoints);
    }
}
