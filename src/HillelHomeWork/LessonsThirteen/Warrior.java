package HillelHomeWork.LessonsThirteen;

import java.util.Random;

public abstract class Warrior {

    protected HealthState healthState = HealthState.GOOD;

    protected int forceLevel;

    protected int defenceLevel;

    public HealthState getHealthState() {
        return healthState;
    }

    public void initForceLevel() {
        this.forceLevel = new Random().nextInt(10);
    }

    public void initDefenceLevel() {
        this.defenceLevel = new Random().nextInt(100);
    }

    protected void changeHealthState(int livePoints) {
        HealthState[] values = HealthState.values();
        for (HealthState value : values) {
            if (livePoints >= value.getMinLevel() && livePoints <= value.getMaxLevel()) {
                healthState = value;
            }
        }
    }
}
