package HillelHomeWork.LessonsThirteen;

public interface Defenceable {

    void defence(int attackPoint);

    default int[] calculateDefenceLevelAndLivePoint(int attackPoints, int defenceLevel, int livePoints) {
        int[] localArray = new int[2];
        if (attackPoints <= defenceLevel) {
            defenceLevel = defenceLevel - attackPoints;
            localArray[0] = defenceLevel;
            localArray[1] = livePoints;
            return localArray;
        }

        int rest = attackPoints - defenceLevel;
        defenceLevel = 0;

        livePoints = Math.max(livePoints - rest, 0);
        localArray[0] = defenceLevel;
        localArray[1] = livePoints;
        return localArray;
    }

}
