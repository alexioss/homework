package HillelHomeWork.LessonsThirteen;

public class Gnome extends Warrior implements Attackable, Defenceable {

    private int livePoints;

    public Gnome(int livePoints) {
        this.livePoints = livePoints;
    }

    @Override
    public int attack() {
        return forceLevel;
    }

    @Override
    public void defence(int attackPoints) {
        int[] localArray = calculateDefenceLevelAndLivePoint(attackPoints, defenceLevel, livePoints);
        defenceLevel = localArray[0];
        livePoints = localArray[1];

        changeHealthState(livePoints);
    }

    @Override
    public String toString() {
        return "Gnome{" +
                "healthState=" + healthState +
                ", livePoints=" + livePoints +
                ", forceLevel=" + forceLevel +
                ", defenceLevel=" + defenceLevel +
                '}';
    }
}
