package HillelHomeWork.LessonsThirteen;

import java.util.ArrayList;

public class Solution {

    public static void main(String[] args) {

        ArrayList<Warrior> armyGnome = new ArrayList<>();
        ArrayList<Warrior> armyElf = new ArrayList<>();

        Solution solution = new Solution();

        solution.createArmy(armyGnome, armyElf);
        solution.initArray(armyGnome);
        solution.initArray(armyElf);
        solution.printArmy(armyGnome);
        solution.printArmy(armyElf);
        System.out.println();

        solution.battle(armyGnome, armyElf);
        solution.printArmy(armyGnome);
        solution.printArmy(armyElf);

    }

    private void printArmy(ArrayList arrayList) {
        for (int i = 0; i < arrayList.size(); i++) {
            System.out.println(arrayList.get(i));
        }
    }

    private void createArmy(ArrayList armyOne, ArrayList armyTwo) {
        for (int i = 0; i < 4; i++) {
            armyOne.add(new Gnome(100));
            armyOne.add(new Gnome(100));
            armyTwo.add(new ElfArcher(100));
            armyTwo.add(new ElfSwordMan(100));
        }
    }

    private void battle(ArrayList armyOne, ArrayList armyTwo) {
        for (int j = 0; j < 100; j++) {
            for (int i = 0; i < armyOne.size(); i++) {
                Warrior warriorOne = (Warrior) armyOne.get(i);
                Warrior warriorTwo = (Warrior) armyTwo.get(i);
                fight(warriorOne, warriorTwo);
                fight(warriorTwo, warriorOne);
            }
        }
    }

    private void fight(Warrior warriorOne, Warrior warriorTwo) {
        int attackLevel = 0;
        if (warriorOne.getHealthState() != HealthState.DEAD && warriorOne instanceof Attackable) {
            Attackable one = (Attackable) warriorOne;
            attackLevel = one.attack();
        }

        if (warriorOne.getHealthState() != HealthState.DEAD && warriorOne instanceof Defenceable) {
            Defenceable two = (Defenceable) warriorTwo;
            two.defence(attackLevel);
        }
    }

    private void initArray(ArrayList arrayList) {
        for (int i = 0; i < arrayList.size(); i++) {
            Warrior warrior = (Warrior) arrayList.get(i);
            warrior.initForceLevel();
            warrior.initDefenceLevel();
        }
    }

}
